import { EUIComponent } from "./UIController";

class BaseComponent {
    public node: { active: boolean } = { active: false };
}

export default class UIComponent extends BaseComponent {
    public id: EUIComponent = null;

    public get isActive(): boolean { return this._isActive } 
    public set isActive(state: boolean) {
        if (this._isActive === state) return;

        this._isActive = state;

        console.log(`\tUI ${EUIComponent[this.id]} 'isActive' set to ${state}`);
    }

    private _isActive: boolean = true;


    public show(): void {
        if (!this.isActive || this.node.active) return;

        this.node.active = true;
        console.log(`\tUI ${EUIComponent[this.id]} shown`);
    }

    public hide(): void {
        if (!this.node.active) return;

        this.node.active = false;
        console.log(`\tUI ${EUIComponent[this.id]} hidden`);
    }    
}