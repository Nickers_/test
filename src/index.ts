import { UIController, EUIComponent } from "./UIController";

class Managers {
    public static UIController: UIController = null;
    public static mainUI: MainUI = null;
}

class MainUI {
    public init() {
        console.log('--------------------------');
        console.log('Сначало покажем весь UI:');

        Managers.UIController.showAll();
        
        console.log('--------------------------');
        console.log('Активируем MoneyBox и деактивируем Lives:');
        
        Managers.UIController.activate(EUIComponent.MoneyBox);

        Managers.UIController.deactivate(EUIComponent.Lives);
        
        console.log('--------------------------');
        console.log('Спрячем весь UI но покажем Lives и Currency:');

        Managers.UIController.hideAll();
        Managers.UIController.show(EUIComponent.Lives);
        Managers.UIController.show(EUIComponent.Currency);

        console.log('--------------------------');
        console.log('Активируем Lives и покажем весь UI:');

        Managers.UIController.activate(EUIComponent.Lives);
        Managers.UIController.showAll();
    }
}

Managers.UIController = new UIController();
Managers.mainUI = new MainUI();

Managers.UIController.init();
Managers.mainUI.init();