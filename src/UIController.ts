import UIComponent from "./UIComponent";

export enum EUIComponent {
    MoneyBox = 501,
    Currency = 502,
    Lives = 503,
}

class MoneyBoxUI extends UIComponent {
    public id: EUIComponent = EUIComponent.MoneyBox;

    public isActive: boolean = false;
}

class CurrencyUI extends UIComponent {
    public id: EUIComponent = EUIComponent.Currency;
}

class LivesUI extends UIComponent {
    public id: EUIComponent = EUIComponent.Lives;
}

export class UIController {
    // @param
    private _UIComponents: UIComponent[] = [ new MoneyBoxUI, new CurrencyUI, new LivesUI ];

    private _MappedUIComponents: Map<EUIComponent, UIComponent> = new Map();

    public init(): void {
        this.mapUI();
    }
    
    public showAll(): void {
        this._UIComponents.forEach(UIComponent => {
            UIComponent.show();
        });
    }

    public hideAll(): void {
        this._UIComponents.forEach(UIComponent => {
            UIComponent.hide();
        });
    }

    public show(id: EUIComponent): void {
        this.forUI(id, (UIComponent) => { 
            UIComponent.show();
        });
    }

    public activate(id: EUIComponent): void {
        this.forUI(id, (UIComponent) => { 
            UIComponent.isActive = true; 
        });
    }

    public deactivate(id: EUIComponent):void {
        this.forUI(id, (UIComponent) => { 
            UIComponent.isActive = false;

            UIComponent.hide();
        });
    }

    private forUI(id: EUIComponent, callback: (UI: UIComponent) => void): void {
        let UIComponent: UIComponent = this._MappedUIComponents.get(id);

        if (!UIComponent) {
            console.error('No such UI', id);

            return;
        }

        callback(UIComponent);
    }

    private mapUI(): void {
        this._UIComponents.forEach(UIComponent => {
            this._MappedUIComponents.set(UIComponent.id, UIComponent);
        });
    }
}